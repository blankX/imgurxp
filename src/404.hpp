#include <kore/kore.h>
#include <kore/http.h>

extern "C" {
    int not_found(struct http_request*);
}
