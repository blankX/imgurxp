#include <string>
#include <optional>
#include <vector>
#include <kore/kore.h>

enum MediaType {
    Image,
    Video
};
typedef struct {
    std::optional<std::string> description;
    MediaType type;
    std::string id;
    std::string ext;
    uint64_t width;
    uint64_t height;
} Media;
typedef struct {
    bool is_mature;
    std::optional<std::string> title;
    std::vector<Media> media;
} Album;
typedef struct {
    std::string key;
    std::string field;
} DeterminedKeys;

std::string build_error_page(const char* error, bool already_escaped = false);
void escape_xml(std::string* text);
// does not handle errors
std::optional<std::string> deserialize_album(kore_json json, Album* album);
DeterminedKeys determine_keys(const char* id, bool album);
void escape_newline(std::string* text);
void unescape_newline(std::string* text);
std::string pack_album_into_string(Album album);
// is not expected to be empty
std::optional<std::string> unpack_album_from_string(Album* album, char* str);
int send_album_page(struct http_request* req, Album album);
