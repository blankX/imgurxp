# ImgurXP

Alternative Imgur frontend written in C++. It is recommended to have a reverse
proxy point to the server with these headers:
```
Strict-Transport-Security: max-age=31536000; includeSubDomains; preload
Content-Security-Policy: default-src 'none'; img-src 'self'; media-src 'self'; style-src 'self' 'unsafe-inline'; sandbox
X-Content-Type-Options: nosniff
Access-Control-Allow-Origin: *
```
