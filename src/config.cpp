#include <kore/kore.h>
#include <fstream>
#include <hiredis/hiredis.h>
#include "config.hpp"

extern "C" {
    void kore_parent_configure(int argc, char* argv[]);
    void kore_worker_configure();
    void kore_worker_teardown();
}

void kore_parent_configure(int argc, char* argv[]) {
    std::ifstream config_fstream("config.json");
    std::string config_str;
    // definitely could be better but i'm not spending more hours on this
    char c;
    while (config_fstream.get(c)) {
        config_str.push_back(c);
    }
    config_fstream.close();
    if (config_fstream.fail() && !config_fstream.eof()) {
        kore_log(LOG_ERR, "fail bit set when reading config");
        exit(1);
    }
    kore_json json;
    kore_json_init(&json, config_str.c_str(), config_str.length());
    if (kore_json_parse(&json) != KORE_RESULT_OK) {
        kore_log(LOG_ERR, "failed to parse json: %s", kore_json_strerror(&json));
        exit(1);
    }
    kore_json_item* redis_host_json = kore_json_find_string(json.root, "redis_host");
    kore_json_item* redis_port_json = kore_json_find_integer_u64(json.root, "redis_port");
    kore_json_item* redis_password_json = kore_json_find_string(json.root, "redis_password");
    if (redis_host_json && redis_port_json && *redis_host_json->data.string && redis_port_json->data.s64) {
        config.redis_host = redis_host_json->data.string;
        config.redis_port = redis_port_json->data.s64;
    }
    if (redis_password_json && *redis_password_json->data.string) {
        config.redis_password = redis_password_json->data.string;
    }
}

void kore_worker_configure() {
    if (config.redis_host && config.redis_port) {
        kore_log(LOG_INFO, "connecting to redis instance at %s:%d", (*config.redis_host).c_str(), (*config.redis_port));
        struct timeval timeout = {1, 0};
        redis = redisConnectWithTimeout((*config.redis_host).c_str(), (*config.redis_port), timeout);
        if (!redis || redis->err) {
            if (redis) {
                kore_log(LOG_ERR, "failed to connect to redis: %s", redis->errstr);
                redisFree(redis);
                redis = nullptr;
            } else {
                kore_log(LOG_ERR, "failed to connect to redis");
            }
            kore_shutdown();
            exit(1);
        }
        if (config.redis_password) {
            redisReply* reply = (redisReply*)redisCommand(redis, "AUTH %s", (*config.redis_password).c_str());
            if (!reply || reply->type == REDIS_REPLY_ERROR) {
                if (!reply) {
                    kore_log(LOG_ERR, "received nullptr while authenticating to redis");
                } else {
                    kore_log(LOG_ERR, "received error while authenticating to redis: %s", reply->str);
                    freeReplyObject(reply);
                }
                redisFree(redis);
                redis = nullptr;
                kore_shutdown();
                exit(1);
            }
        }
    }
}

void kore_worker_teardown() {
    if (redis) {
        redisFree(redis);
    }
}
