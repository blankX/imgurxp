#include <string>
#include <optional>
#include <mutex>
#include <hiredis/hiredis.h>

typedef struct {
    std::optional<std::string> redis_host;
    std::optional<int> redis_port;
    std::optional<std::string> redis_password;
} Config;

inline Config config;
inline redisContext* redis = nullptr;
inline std::mutex redis_mutex;
