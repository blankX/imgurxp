#include <kore/kore.h>
#include <kore/http.h>
#include "assets.h"
#include "404.hpp"

int not_found(struct http_request* req) {
    http_response_header(req, "Content-Type", "text/html");
    http_response(req, 404, asset_404_html, asset_len_404_html);
    return KORE_RESULT_OK;
}
